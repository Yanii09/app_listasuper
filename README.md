# README #
Este repositprio se lo utiliza para llevar a cabo un registro de lo realizado en el trabajo final, que debe contar con una aplicación creada en App Inventor y para realizar un trabajo en equipo, coordinando las tareas usamos la aplicación Trello, que nos permite organizarnos y qué tareas le corresponde a cada integrante del grupo.
El objetivo de la app es, facilitar llevar un registro de lo que se debe adquirir cuando se realizan las compras de productos y alimentos necesarios para abastecer un hogar, y no olvidarse de nada indispensable.

Creamos el proyecto DB_Lista_de_Supermercado con la siguiente interfaz:


Screen1: Le insertamos una etiqueta donde le pusimos el nombre de la App "LISTA DE SUPERMERCADO", pusimos una Disposición Horizontal donde arrastramos un "Campo de Texto" que nos va a servir para escribir los productos que necesitamos comprar, y un botón que lo renombramos "AGREGAR" que nos sirva para insertar el texto del campo de texto en la Listview. Le pusimos un dispositivo de almacenamiento TinyDB que nos va a servir para generar nuestra BD y una Listview donde se irán cargando los productos de la lista del supermercado. También le insertamos un Botón que lo llamamos "BORRAR PRODUCTO", el cual borra el producto de la lista que no nos sirve o que lo ingrsamos por error, y un Botón "VER PRODUCTO" que nos dirige a la Screen2 "PRODUCTOS", donde se mostrará el producto seleccionado en la Listview y cuenta con un Botón "ATRÁS" para regresar a la pantalla principal.
En el archivo funcionalidad_app, se describe el paso a paso de lo realizado con los bloques. Dejando en claro las disposiciones usadas, los widgets empleados de la paleta de trabajo y definimos los bloques para que realicen determinadas funciones.



Imágenes: las imágenes que adjuntamos son relacionadas a productos de supermercado, las cuales las descargamos en formato jpg o png. Por otra parte, se generaron botones que fueron utilizadas en la app, donde fueron realizadas en el siguiente link                                  https://www.clickminded.com/button-generator/  . También, para cambiar el color de fondo de las imágenes usamos la aplicación Remove Background (removebg), y así utilizarlo con un fondo transparente.
El repositorio cuenta con 3 carpetas de imágenes, hay 2 carpetas que contienen las imágenes de los botones y del fondo usados, cada una se encuentra en sus respectivas carpetas. Y una última carpeta, donde se adjuntó las imágenes de las pantallas de la app, para mostrar como se visualiza la aplicación en el celular.


Este es el link de Trello: https://trello.com/b/y03CHQVC/trabajo-final .

En la carpeta app_inventor, se adjunto los archivos apk y .aia de la app_listasuper que estuvimos trabajando en grupo.

Lo que decidimos hacer, para no crear otro Trello, fue armar nuevas etiquetas de tareas para ir anotando ahí lo que vamos haciendo en el nuevo repositorio "app_listasuper". Y dejamos en la columna "TAREAS CON PROBLEMAS" el repositorio anterior en el cual no pudimos seguir trabajando por diversos inconvenientes. 



